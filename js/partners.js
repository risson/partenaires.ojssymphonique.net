$(document).ready( function() {
    var $partnersContainer      = $('.container-partners');
    var $wrapperContainer       = $('.wrapper-partners');
    var $wrapperContainerCopy;

    var blockPartnerMarginRight = 75;

    var screenWidth;
    var intervalAnimation;
    var currentMarginParntersContainer;

    var animationPartnerOn = true;
    var settingInProgress  = true;

    var widthPartnersContainer;
    var widthWrapperContainer;

    initValuesPartnersSlider();

    function initValuesPartnersSlider() {
        screenWidth            = $(document).width();
        widthPartnersContainer = $partnersContainer.width();
        widthWrapperContainer  = $wrapperContainer.width();

        $partnersContainer.empty();
        $partnersContainer.append($wrapperContainer);

        currentMarginParntersContainer = blockPartnerMarginRight/2;
        animationPartnerOn             = true;

        $partnersContainer.css('margin-left', currentMarginParntersContainer.toString() + "px");

        intervalAnimation      = null;

        if (widthWrapperContainer > 0 ) {
            if (widthWrapperContainer > screenWidth) {
                if (!$('.wrapper-partners-copy').length ) {
                    // Do something
                    cloneWrapper();
                }

                setIntervalPartnersAnimation();
            } else {
                centerPartnersContainer();
            }
        }
        settingInProgress = false;
    }

    function cloneWrapper() {
        $wrapperContainerCopy = $wrapperContainer.clone();
        $wrapperContainerCopy.addClass('wrapper-partners-copy');
        $wrapperContainerCopy.removeClass('wrapper-partners');
        $partnersContainer.append($wrapperContainerCopy);
    }

    function setIntervalPartnersAnimation() {
        intervalAnimation = $(function() {

            if (animationPartnerOn == true) {
                moveSlide();
            }

            function moveSlide() {
                if (animationPartnerOn == true) {
                    if (Math.abs(currentMarginParntersContainer) > widthWrapperContainer) {
                        currentMarginParntersContainer = blockPartnerMarginRight/2;
                        $partnersContainer.css('margin-left', currentMarginParntersContainer.toString() + "px");
                    } else {
                        currentMarginParntersContainer = currentMarginParntersContainer - 600;
                        $partnersContainer.css('margin-left', currentMarginParntersContainer.toString() + "px");
                    }

                    if (animationPartnerOn == true) {
                        setTimeout(moveSlide, 2000);
                    }
                }
            }
        });
    }

    function removeIntervalPartnersAnimation() {
        animationPartnerOn = false;
    }

    function centerPartnersContainer() {
        var newMarginLeft = (screenWidth - widthWrapperContainer + blockPartnerMarginRight) / 2;

        $partnersContainer.css('margin-left', newMarginLeft.toString() + "px");
    }

    // on resize calculate it again
    $( window ).resize(function() {
        if (settingInProgress == false) {
            removeIntervalPartnersAnimation();

            if (animationPartnerOn == false) {
                settingInProgress = true;
                setTimeout(initValuesPartnersSlider, 2000);
            }
        }
    });

});